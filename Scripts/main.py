#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  8 17:08:07 2021

@author: guetin gael
"""
#Bibliothèques
import numpy as np

import math

from scipy.stats import mode

from mpl_toolkits import mplot3d

from math import *

from cmath import *

import tensorflow as tf

from tensorflow import keras

from tensorflow.keras import layers

from tensorflow.keras.models import Sequential, save_model, load_model

from tensorflow.keras.layers import Dense

from tensorflow.keras.losses import binary_crossentropy

from tensorflow.keras.optimizers import Adam
 
import matplotlib.pyplot as plt

from random import *

from Calc_multicouches_mesures_epaisseurs import *

from Multicouches_datas_test import *

######Paramètres à changer pour générer les datas ou entrainer le RN

Generate = False #Generate à True pour generer les datas sinon False
Reseau = True  #Reseau à True pour entrainer le Réseau sinon False

###################Paramètres du multicouche :Datas de train

Indices_real_min = 1.00
Indices_real_max = 1.2
Epaisseurs_min = 0.001
Epaisseurs_max = 0.01
Indices_imag_min = 0.01
Indices_imag_max = 0.1

nombre_de_points = 10000
nombre_de_samples = 12000

min_couches = 2

max_couches = 8

offset_dans_modelisation = 8000

vitesse_lumiere = 2.9978e8

###################################################### Paramètres réseau de neurones

no_epochs=5

inputs = (nombre_de_points,1) #Entrée de (10000,1)

batch_size = 250 #Taille du lot est de 250:Bon équilibre entre pertes de valeurs et temps de prédiction

train_split = 0.1 #Utilisation de 10% des données totales comme data de train

verbosity = 1 # Vebose niveau 1: affiche les sorties d'informations complètes

validation_split = 0.2

num_samples = 2000

num_predictions = 2 #nombre de prédictions: A faire varier en fonction de la taille du vecteur des indices de prédiction


if (Generate):
    ###########################################Formation du jeu de données
    binary_data = np.zeros((nombre_de_samples,nombre_de_points),dtype=float)
    
    normalize_data=np.zeros((nombre_de_samples,nombre_de_points),dtype=float)
    
    data_1 = np.zeros((nombre_de_samples,1),dtype=float)
    
    data_2 = np.zeros((nombre_de_samples,1),dtype=float)
    
    data_3 = np.zeros((nombre_de_samples,1),dtype=float)

    #Calcul de la transformation de Fourier
    Refl = np.loadtxt('RawCalibrationRefl.txt') # Signal du reflecteur
    nc = len(Refl) #Nombre de points dans le signal (doit être égal au nombre de points dans le signal de référence)

    top_index = nc
    bottom_index = 0

    ######################Modélisation

    fft_Refl = fft(Refl)
    fft_Refl = fft_Refl[bottom_index:top_index] #Récupération d'une partie de la fft du reflecteur
    fft_Refl_1 = fft_Refl.reshape(1,-1)

    x= np.zeros((1,np.size(fft_Refl)))

    ###############################Position des interfaces
    Index_Refl = np.argmax(abs(Refl))

    Ep_air = np.array([120e-6])

    Re_Sub = 1  # Partie réelle substrat  (ne pas modifier)

    # Im_Sub = 1000							  # Partie imaginaire substrat (ne pas modifier)

    Im_Sub = 0  # Partie imaginaire substrat modifiée à 0 pour éviter les échos

    fmin = 100e9 # Fréquence mimimale

    fmax = 4000e9 # Fréquence maximale

    Step_t_brut = 0.05e-12 # Pas temporel

    Step_f_brut = 1 / (Step_t_brut * (nc - 1))

    Step_metric = Step_t_brut * vitesse_lumiere / 2

    #######################Calcul de la fréquence grace a la boucle for

    for i in range(0, nc + 1):

        for j in range(0, x.shape[0]):
            
            x[j, i - 1] = Step_f_brut * i
    f = x

    Field_save = np.zeros((1, np.size(f)), dtype=complex)

    for i in range(1, np.size(f)):
        
        Air = Calc_multicouches_mesures_epaisseurs(1, Ep_air, np.array([[1, 0]]), np.array([1, 1000]), f[0, i])
        
        Field_save[0, i] = Air[0]
 
    for k in range(0,nombre_de_samples):
    ##########################Génération aléatoire des couches
        nLayers = randint(min_couches,max_couches)

            ###########################Génération aléatoire des index du multicouche
        Index_mat_complex = np.zeros((nLayers,2))

        for i in range(0,nLayers):

            Index_mat_reel = uniform(Indices_real_min, Indices_real_max)

            Index_mat_im = uniform(Indices_imag_min, Indices_imag_max)

            Index_mat_complex[i]= np.array([Index_mat_reel, Index_mat_im])

        Index_mat = Index_mat_complex

        ###########################Génération aléatoire des épaisseurs des couches
        Ep_layer = np.zeros((len(Index_mat)))

        for i in range (0, nLayers):

            Ep_lay_al = uniform(Epaisseurs_min, Epaisseurs_max)

            Ep_layer[i] = np.array([Ep_lay_al])

        Ep_layer = Ep_layer


        H_mod = np.zeros((1,np.size(f)),dtype=complex)

        for i in range(1,np.size(f)):
            
            Field = Calc_multicouches_mesures_epaisseurs(nLayers,Ep_layer,Index_mat,np.array([Re_Sub ,Im_Sub]),f[0,i])
            
            H_mod[0,i] = np.conj(Field[0]/Field_save[0, i])*fft_Refl_1[0,i]
      
        modelisation = np.fft.ifft(H_mod)   # Signal modélisé à partir des paramètres du multicouche

        signal_mod = modelisation.reshape(-1,1)

        signal_mod  = abs(signal_mod[offset_dans_modelisation:offset_dans_modelisation+nombre_de_points,:])

        #####################################Calcul du temps de vol entre la position 0 et position max du reflecteur

        Temps_vol = Index_Refl * Step_t_brut

        ############Position des interfaces
        deltaT = np.zeros((len(Ep_layer)+1,1))

        Index_vol= np.zeros((len(Ep_layer)+1,1),dtype=int)

        deltaT[0] = Temps_vol

        Index_vol[0] = Temps_vol/Step_t_brut

        if (len(Ep_layer)>1):

            for i in range(0,len(Ep_layer)):
         
                deltaT[i+1] = deltaT[i] + (2*Ep_layer[i]*Field[4][0,i])/vitesse_lumiere

                Index_vol[i+1] = floor(deltaT[i+1]/Step_t_brut)

        idxDeltaT = deltaT/Step_t_brut

        Index =  Index_vol #Calcul de l'index

        signal_normalized = signal_mod/max(signal_mod)

        B_mod = np.zeros((25000,1),dtype=bool)#Initialisation du vecteur binaire(booleen)

        for i in range(0,len(Ep_layer)+1):
              
            B_mod[Index[i]] = True

        B_mod = B_mod[offset_dans_modelisation:offset_dans_modelisation+nombre_de_points,:]

        binary_sig = B_mod.reshape(1,-1)

        normalize_sig = (signal_normalized.reshape(1,-1))

        binary_data[k] = np.concatenate(binary_sig)

        normalize_data[k]  = np.concatenate(normalize_sig)
        
        data_1[k] = np.array([Index_mat_reel] )
        
        data_2[k] = np.array([Index_mat_im])
        
        data_3[k] = np.array([Ep_lay_al])

        print('k vaut :',k)

    binary_data = binary_data

    normalize_data = normalize_data
    
      ###Formations des datas pour les indices et les epaisseurs
    data_1 = data_1
    
    data_2 = data_2
    
    data_3 = data_3
    
    ########################################Formation du réseau de neurones
    
    ##Datas de train
    
    val_noisy  = binary_data #Signal binaire
    
    val_pure = normalize_data #Signal modélisé
    
    ##Datas de test
      
    val_noisy_test, val_pure_test = Multicouches_datas_test()
      
    ############Sauvegarde des datas
    
      ##Datas de train

    np.save('./value_noised.npy',val_noisy)

    np.save('./value_pure.npy',val_pure)
    
      ##Datas de test
    
    np.save('./value_noised_test.npy',val_noisy_test)
    
    np.save('./value_pure_test.npy',val_pure_test)
   
else:
###########Chargement des datas

    ##Datas de train

    val_noisy = np.load('./value_noised.npy')
    
    val_pure = np.load('./value_pure.npy')
    
    ##Datas de test 
    
    val_noisy_test = np.load('./value_noised_test.npy')
    
    val_pure_test = np.load('./value_pure_test.npy')

    ##Datas pour prédiction
    
    val_test_pred = np.load('./data_test_pred.npy')
#####################Formation des datas 3D

#########Reechantillonage des datas

  #Datas de train

noisy_input = val_noisy.reshape((val_noisy.shape[0], val_noisy.shape[1], 1))

pure_input =  val_pure.reshape((val_pure.shape[0], val_pure.shape[1], 1))

  #Datas de test 
      
noisy_input_test = val_noisy_test.reshape((val_noisy_test.shape[0], val_noisy_test.shape[1], 1))

pure_input_test =  val_pure_test.reshape((val_pure_test.shape[0], val_pure_test.shape[1], 1))

Step_t_brut = 0.05e-12
vol = np.zeros((10000,1), dtype =float)

for i in range (0,10000):
    
    vol[i]= i*Step_t_brut
    
idx_t = vol

    ##Datas pour prédiction
data_test_pred = val_test_pred.reshape((val_test_pred.shape[0], val_test_pred.shape[1], 1))

###########################################Création de l'architecture du modèle : Multilayers perceptron
if (Reseau):
    model = Sequential()
    
    model.add(Dense(32, activation="relu",input_shape = inputs))

    model.add(Dense(16, activation="relu"))

    model.add(Dense(1,activation ="relu"))
     
    #Résumé du modèle

    model.summary()

    #Compilation du modèle

    model.compile(optimizer="adam", loss='binary_crossentropy', metrics = ['accuracy'])

    #Entrainement du modèle

    model.fit(noisy_input, pure_input, epochs = no_epochs, verbose = verbosity, validation_split = validation_split)

    #Evaluation du modèle
    score = model.evaluate(pure_input_test, noisy_input_test, verbose = 0)
    
    print('Test_loss :', score[0])
    
    print('Test_accuracy :', score[1])
    
    #Sauvegarde du modèle
    filepath = './saved_model'
    
    save_model(model, filepath)
    
    #Chargement du modèle
    model = load_model(filepath, compile = True)
    
##############################Visualisation des datas

#Génération des prédictions: Prédiction du modèle

    samples = noisy_input_test[:num_samples]
    
    waves = pure_input_test[:num_samples]
    
    predictions = model.predict(data_test_pred)

##Afficher les prédictions et les signaux binaires correspondants aux signaux modélisés
if (True):
    for i in range(0, num_predictions):
        
        print("hello")
        
        if (Reseau):
            prediction = np.array(predictions[i])

            plt.figure(1)
        
        plt.title('Visualisation signal modélisé, position des interfaces obtenues via modélisation et prédiction du réseau')
        
        bin_sample = np.array(samples[i])

        original = np.array(waves[i])
        
        if (Reseau):
            plt.plot(idx_t,prediction, 'k', label ='p', linewidth = 1.0)

        plt.plot(idx_t,original,'g', label ='t', linewidth = 0.5)#Signal modélisé

        plt.plot(idx_t,bin_sample,'b', label ='i', linewidth = 0.25)#Signal binaire

        plt.xlabel('Temps (ps)')

        plt.ylabel('Amplitude')
        
        plt.legend(loc='upper right')

        plt.show()

########################Calcul des écarts entre signaux binaires et signaux prédits

if (Reseau):
    ###Variabes
    column_size = 1
    samples_size = 18000 ##A faire varier pour stocker les indices
    
    Tab_test_save = np.zeros((samples_size,column_size),dtype=int)
    
    Tab_prediction_save = np.zeros((samples_size,column_size),dtype=int)
    
    val_test = val_noisy_test[0].reshape(-1,1)
    
    Step_t_brut = 0.05e-12
    
    inc = 0
    ic = 0
    ##############Recupération des index des valeurs de test
    for k  in range (0,  len(val_noisy_test)):
        
        for i in range (0, len(val_test)):
        
            if (val_noisy_test[k][i]==1.0):
        
                Tab_test_save[inc] = i
                   
                inc = inc + 1
    
    Tab_test_save =  Tab_test_save 
    
    Tab_test = Tab_test_save * Step_t_brut
    
    ###############Recupération des index des valeurs de prédiction
    for j  in range (0,  len(predictions)): 
        
        for i in range(0, len(val_test)):
            
            if (predictions[j][i] > 0.01):
            
                Tab_prediction_save[ic] = i
            
                ic = ic + 1
                
                print("Nombre total d'interfaces =",ic)
       
    Tab_prediction_save =  Tab_prediction_save
  
    Tab_pred = Tab_prediction_save * Step_t_brut 
           
    ###############Calcul de l'ecart temporel entre valeurs de test et valeurs de prediction    
    Tab_ecart =  Tab_test - Tab_pred 
    
    Tab_ecart_index = Tab_test_save - Tab_prediction_save
    ####################Calculs statistiques
    
    ###############Calcul de la moyenne d'une loi normale
    Moyenne = np.mean(Tab_ecart)
    
    ##############Calcul de la variance d'une loi normale
    Variance = np.var(Tab_ecart)
    
    ############## Calcul de l'écart type d'une loi normale
    Ecart_type = np.std(Tab_ecart)
    
    ###Calcul de la médiane d'une loi normale
    
    Median_value = np.median(Tab_ecart) 
    
    ###Calcul de la somme
    
    Somme_value = np.sum(Tab_ecart)
    
    ###Calcul du mode d'une loi normale : Fréquence de repétition d'un nombre
    
    Mode_value = mode(Tab_ecart)

    ###Calcul de l'ecart max

    Ecart_max = np.max(Tab_ecart)
##################Affichage des signaux modélisés et binaires de train

plt.figure(2)

plt.title('Visualisation du signal modélisé et binaire')

plt.plot(idx_t,val_noisy[0],'b',linewidth = 0.5)

plt.plot(idx_t,val_pure[0],'r',linewidth = 0.25)

plt.xlabel('Temps (ps)')

plt.ylabel('Amplitude')

plt.show()
    
if (Generate):

    ####Affichage des indices et des épaisseurs des couches en nuage de points: Datas de train
    plt.figure(3)
    
    ax = plt.axes(projection ='3d')
    
    d_plot_1 = data_1
    
    d_plot_2 = data_2
    
    d_plot_3 = data_3
    
    ax.scatter3D(d_plot_1, d_plot_2, d_plot_3, c ='green')
    
    ax.set_title(' Tracé des variables de train en 3D')
    
    ax.set_xlabel("Index_mat_reel")
    
    ax.set_ylabel("Index_mat_im")
    
    ax.set_zlabel("Ep_layer")
    
    plt.show()

if (Reseau):
    ##############Génération de l'histogramme d'écarts
    plt.figure(4)
    
    plt.hist(Tab_ecart, color = 'red', edgecolor = 'black', alpha=0.7, rwidth=0.85)
    
    plt.xlabel('Temps (ps)')
    
    plt.ylabel('Nombre total des interfaces trouvées')
    
    plt.title('Histogramme des écarts')
    