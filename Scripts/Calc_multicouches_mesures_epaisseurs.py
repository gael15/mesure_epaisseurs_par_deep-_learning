#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  8 17:32:05 2021

@author: guetin gael
"""
from math import *

from cmath import *

import math

import numpy as np

from numpy.fft import fft, ifft

import matplotlib.pyplot as plt

def Calc_multicouches_mesures_epaisseurs(Nb_Layer,Ep,Index_mat,Index_subst,Freq):
    
    j=complex(0,1)
    
    Index_super=1                                              # Introduction des paramètres de substrat et de superstrat
    
    Index_sub=Index_subst[0]+ j*Index_subst[1]
    
    Lambda=2.997e8/(Freq)
    
    Puls=2*pi*Index_super*sin((pi/180)*8)/Lambda
    
    Ki_sub=2*pi*Index_sub/Lambda                              # Calcul de l'indice effectif du substrat
    
    Alpha_sub=sqrt(Ki_sub**2-Puls**2)
    
    Index_Eff_sub=Index_sub*Alpha_sub/Ki_sub 
    
    Index = np.zeros((1,Nb_Layer),dtype=complex)
    
    Ki = np.zeros((1,Nb_Layer),dtype=complex)
    
    Alpha = np.zeros((1,Nb_Layer),dtype=complex)
    
    Index_Eff = np.zeros((1,Nb_Layer),dtype=complex)
    
    Phase = np.zeros((1,Nb_Layer),dtype=complex)
    
    for i in range(0,Nb_Layer):                                            # Calcul de l'indice effectif et de la variation de phase pour chaque couche
        #print(i)
        Index[0,i]=Index_mat[i,0]+i*Index_mat[i,1]           # Indice complexe de chaque couche
        
        Ki[0,i]=2*pi*Index[0,i]/Lambda
        
        Alpha[0,i]=sqrt(Ki[0,i]**2-Puls**2)                      # Composante normale du vecteur d'onde pour chaque couche
        
        Index_Eff[0,i]=Index[0,i]*Ki[0,i] / Alpha[0,i] # Indice effectif pour chaque couche en TE
        
        Phase[0,i]=Alpha[0,i]*Ep[i]                           # Calcul de la variation de phase dans chaque couche
    
    Y=Index_Eff_sub  
                                          # Calcul de l'admittance complexe du multicouche
    Prod=1
    
    for i in range (Nb_Layer-1,-1,-1):
        
        #print(i)
        
        Prod=Prod*(cos(Phase[0,i])-j*Y*sin(Phase[0,i])/Index_Eff[0,i])
        
        Y=(-j*Index_Eff[0,i]*sin(Phase[0,i])+Y*cos(Phase[0,i]))/(cos(Phase[0,i])-j*Y*sin(Phase[0,i])/Index_Eff[0,i])
    
    r0=((Index_super-Y)/(Index_super+Y))                         #Calcul des champs incident et transmis et des intensités associées
   
    R0=abs(r0)**2
    
    t0=(1+r0)/Prod
    
    T0=np.real(Index_Eff_sub)*abs(t0)**2/Index_super
    
    Field=np.array([r0,t0,R0,T0,Index_Eff])

    return Field