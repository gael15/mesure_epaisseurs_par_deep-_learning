# -*- coding: utf-8 -*-
"""
Created on Tue Jul 27 16:59:08 2021

@author: gael.guetin
"""
#Bibliothèques
import numpy as np

import math

from math import *

from cmath import *

import matplotlib.pyplot as plt

from random import *

from Calc_multicouches_mesures_epaisseurs import *

#####################################################Paramètres du multicouche :Datas de prédiction
Indices_real_min = 1.00
Indices_real_max = 1.2
Epaisseurs_min = 0.001
Epaisseurs_max = 0.01
Indices_imag_min = 0.01
Indices_imag_max = 0.1

nombre_de_points = 10000

nombre_de_samples = 2000

min_couches = 2

max_couches = 8

offset_dans_modelisation = 8000

vitesse_lumiere = 2.9978e8

###########################################Formation du jeu de données
data_test_pred = np.zeros((nombre_de_samples,nombre_de_points),dtype=bool)


#Calcul de la transformation de Fourier
Refl = np.loadtxt('RawCalibrationRefl.txt') # Signal du reflecteur
nc = len(Refl) #nombre de points dans le signal (doit être égal au nombre de points dans le signal de référence)

top_index= nc
bottom_index= 0
  
  ######################Modélisation
  
fft_Refl= fft(Refl)
fft_Refl = fft_Refl[bottom_index:top_index] #Récupération d'une partie de la fft du reflecteur
fft_Refl_1 = fft_Refl.reshape(1,-1)

x= np.zeros((1,np.size(fft_Refl)))

###############################Position des interfaces
Index_Refl = np.argmax(abs(Refl))

Ep_air = np.array([120e-6])

Re_Sub = 1  # Partie réelle substrat  (ne pas modifier)

# Im_Sub = 1000							  # Partie imaginaire substrat (ne pas modifier)

Im_Sub = 0  # Test Partie imaginaire substrat (ne pas modifier)

fmin = 100e9

fmax = 4000e9

Step_t_brut = 0.05e-12

Step_f_brut = 1 / (Step_t_brut * (nc - 1))

Step_metric = Step_t_brut * vitesse_lumiere / 2

#######################Calcul de la fréquence grace a la boucle for sur 25000 points

for i in range(0, nc + 1):

    for j in range(0, x.shape[0]):
        x[j, i - 1] = Step_f_brut * i
f = x

Field_save = np.zeros((1, np.size(f)), dtype=complex)

for i in range(1, np.size(f)):
    Air = Calc_multicouches_mesures_epaisseurs(1, Ep_air, np.array([[1, 0]]), np.array([1, 1000]), f[0, i])
    Field_save[0, i] = Air[0]
     
for m in range(0,nombre_de_samples):
        ##########################Génération aléatoire des couches
    nLayers = randint(min_couches,max_couches)
        
        ###########################Génération aléatoire des index du multicouches
    Index_mat_complex = np.zeros((nLayers,2))
    
    for i in range(0,nLayers):
    
        Index_mat_reel = uniform(Indices_real_min, Indices_real_max)
    
        Index_mat_im = uniform(Indices_imag_min, Indices_imag_max)
    
        Index_mat_complex[i]= np.array([Index_mat_reel, Index_mat_im])
    
    Index_mat = Index_mat_complex
    
    ###########################Génération aléatoire des épaisseurs des couches
    Ep_layer = np.zeros((len(Index_mat)))
    
    for i in range (0, nLayers):
    
        Ep_lay_al = uniform(Epaisseurs_min, Epaisseurs_max)
    
        Ep_layer[i] = np.array([Ep_lay_al])
    
    Ep_layer = Ep_layer
    
    
    H_mod = np.zeros((1,np.size(f)),dtype=complex)
    
    for i in range(1,np.size(f)):
        Field=Calc_multicouches_mesures_epaisseurs(nLayers,Ep_layer,Index_mat,np.array([Re_Sub ,Im_Sub]),f[0,i])
        H_mod[0,i] = np.conj(Field[0]/Field_save[0, i])*fft_Refl_1[0,i]
        
    modelisation = np.fft.ifft(H_mod)   # Signal modélisé à partir des paramètres du multicouche
    
    signal_mod = modelisation.reshape(-1,1)
    
    signal_mod  = abs(signal_mod[offset_dans_modelisation:offset_dans_modelisation+nombre_de_points,:])
    
    #####################################Calcul du temps de vol entre la position 0 et position max du reflecteur
    
    Temps_vol = Index_Refl * Step_t_brut
    
    ############Position des interfaces
    deltaT = np.zeros((len(Ep_layer)+1,1))
    
    Index_vol= np.zeros((len(Ep_layer)+1,1),dtype=int)
    
    deltaT[0] = Temps_vol
    
    Index_vol[0] = Temps_vol/Step_t_brut
    
    if (len(Ep_layer)>1):
    
        for i in range(0,len(Ep_layer)):
       
            deltaT[i+1] = deltaT[i] + (2*Ep_layer[i]*Field[4][0,i])/vitesse_lumiere
        
            Index_vol[i+1] = floor(deltaT[i+1]/Step_t_brut)
        
    idxDeltaT = deltaT/Step_t_brut
    
    Index =  Index_vol #Calcul de l'index
    
    signal_normalized = signal_mod/max(signal_mod)
    
    B_mod = np.zeros((25000,1),dtype=bool)#Initialisation du vecteur binaire(booleen)
    
    for i in range(0,len(Ep_layer)+1):
          
        B_mod[Index[i]] = True
    
    B_mod=  B_mod[offset_dans_modelisation:offset_dans_modelisation+nombre_de_points,:]
    
    binary_sig = B_mod.reshape(1,-1)
    
    data_test_pred[m] = np.concatenate(binary_sig)
    
    print('m vaut :',m)
 
data_test_pred = data_test_pred

np.save('./data_test_pred.npy',data_test_pred)

    


